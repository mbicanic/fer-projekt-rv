# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 20:55:34 2019

@author: mirob
"""
import os

#image_directory - direktorij u kojem se nalaze slike za koje zelite generirati
#                   datoteku koja sadrzi puteve do svih tih slika
#dest_file       - datoteka (otvorena u 'w' nacinu rada) u koju ce se upisati 
#                   putevi do svih slika iz direktorija
#                   Ime datoteke bi trebalo biti "evalpaths.txt" ako se radi o slikama
#                   koje se koriste za evalucaciju, ili "trainpaths.txt" ako 
#                   se radi o slikama za treniranje.
#                   Moguca su i druga imena, ali onda to morate promijeniti i u 
#                   /src/config/coco.data
def generate_paths(image_directory, dest_file):
    paths=[]
    for file in os.listdir(image_directory):
        
        #VAZNO: ispisite os.path.abspath(file) i provjerite je li tocan
        #       Meni je ignorirao 2 foldera na putu, pa je umjesto
        #       .../project/coco/data/images/train2014/<ime_slike>
        #       ispisivao: .../project/coco/data/<ime_slike>
        #       
        #       Problem se jednostavno rjesava pomocu:
        #line = line.replace("/data/", "/data/images/train2014/")
        line = os.path.abspath(file)
        paths.append(line)
    
    for p in paths:
        dest_file.write(p+"\n")
    dest_file.close()
        