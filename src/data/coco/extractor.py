
import os
import os.path
import cv2

car_class = "2"
lbl_path="./labels/val2014/"

for filename in os.listdir(lbl_path):
	#Vidi skriptu generate_paths.py za napomenu vezanu uz os.path.abspath()
    fname = os.path.abspath(filename)
	#print(fname) #provjerite ispisuje li ovo tocnu putanju
    l_file = open(fname, 'r')
    labels = [line for line in l_file.readlines() if (line[0]==car_class and line[1]==" ")]
    if(len(labels)==0):
        continue
    img_fname = fname.replace("/labels/", "/images/").replace(".txt", ".jpg")
    img = cv2.imread(img_fname, cv2.IMREAD_COLOR)
    dest_img_fname = img_fname.replace("/val2014/", "/cars/").replace("/","\\")
    cv2.imwrite(dest_img_fname, img)
        