#!/bin/bash
# Download weights for vanilla YOLOv3
wget -c --no-check-certificate https://pjreddie.com/media/files/yolov3.weights
# Download weights for backbone network
wget -c --no-check-certificate https://pjreddie.com/media/files/darknet53.conv.74
